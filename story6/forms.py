from django import forms

class KegiatanCreateForm(forms.Form):
    nama_kegiatan = forms.CharField(label = 'Nama Kegiatan :', max_length=50, required = True, 
    widget = forms.TextInput(attrs={'class' : 'form-control'}))

class PesertaCreateForm(forms.Form):
    nama_peserta = forms.CharField(label = 'Nama Peserta :', max_length=50, required = True, 
    widget = forms.TextInput(attrs={'class' : 'form-control'}))