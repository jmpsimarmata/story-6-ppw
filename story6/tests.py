from django.test import TestCase, Client
from django.urls import resolve
from .views import landing
from . import models
from .views import indexStory6
from .models import Kegiatan
from .models import Peserta

class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_app_jadwal(self):
        found = resolve('/')
        self.assertEqual(found.func, indexStory6)

    def test_model1_cek(self):
        Kegiatan.objects.create(nama_kegiatan='pepew')
        hitungJumlah = Kegiatan.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_model2_cek(self):
        Peserta.objects.create(nama_peserta='pepew')
        hitungJumlah = Peserta.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    def test_model_name (self):
        Kegiatan.objects.create(nama_kegiatan='pepew')
        kegiatan = Kegiatan.objects.get(nama_kegiatan='pepew')
        self.assertEqual(str(kegiatan), 'pepew')
    
    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
