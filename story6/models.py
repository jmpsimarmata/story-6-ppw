from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 50, default = "")

    def __str__(self):
        return self.nama_kegiatan

class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete= models.DO_NOTHING)
    nama_peserta = models.CharField(max_length = 50)

    def __str__(self):
        return self.nama_peserta